var express = require('express');
var bodyParser = require('body-parser');
const path = require('path')
const hoganMiddleware = require('hogan-middleware');
var product = require('./routes/product'); // Imports routes for the products
var cars = require('./routes/cars');
var app = express();

// ---------------------
app.set('views', path.join(__dirname,'views'))
app.set('view engine','mustache')
app.engine('mustache', hoganMiddleware.__express)
// app.engine('mustache', require('hogan-middleware').__express)
app.use(express.static(path.join(__dirname,'public')))

// -----------------------
// Set up mongoose connection
var mongoose = require('mongoose');
// var dev_db_url = 'mongodb://someuser:abcd1234@ds123619.mlab.com:23619/productstutorial';
const dev_db_url = "mongodb://127.0.0.1:27017/productApp";
var mongoDB = dev_db_url;
mongoose.connect(mongoDB);
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));



app.use('/products', product);
app.use('/examples',cars);

var port = 3000;

app.listen(port, () => {
    console.log('Server is up and running on port numner ' + port);
});
